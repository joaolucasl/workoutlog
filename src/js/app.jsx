import React from 'react';
import DOM from 'react-dom';

import WorkoutApp from './components/WorkoutApp.jsx';

DOM.render(
  <WorkoutApp />,
  document.getElementById('WorkoutApp')
);
